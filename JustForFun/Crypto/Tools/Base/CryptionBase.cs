﻿namespace Crypto.Tools.Base
{
    public abstract class CryptionBase : ICryptionBase
    {
        private protected string Message { get; set; }

        private protected string Result { get; set; } = "";

        public CryptionBase(string message) => Message = message; 

        public bool IsEmpty(string text) => text.Trim(' ') == "";
        public virtual string ReturnResultMessage(bool isEncryption) => Result;
        public string[] SplitMessage(string text) => text.Split("");
    }
}
