﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using Crypto.Tools.Extensions;

namespace Crypto.Tools.Base
{
    public abstract class KeyCryption : CryptionBase, IKeyCryption
    {
        public KeyCryption(string key, string message) : base(message)
        {
            (Key, Message) = (key,message);

            if (!(IsTextValid(Key) && IsTextValid(Message)))
            {
                throw new Exception("There is invalid Message or Key!");
            }

            (MessageIndices, KeyIndices) = (Message.ToInts(Symbols), Key.ToInts(Symbols));
        }

        public string Key { get; set; }

        private protected string Symbols { get; set; } = string.Concat(Enumerable.Range(' ', 94).ToArray().Select(x => (char)x)); // from ' ' to '}'
        public int[] MessageIndices { get; set; }
        public int[] KeyIndices { get; set; }

        private bool IsTextValid(string text) => 
            Key.All(keyChar => Symbols.Contains(keyChar));
    }
}