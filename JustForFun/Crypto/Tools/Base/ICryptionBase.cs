﻿using System.Drawing;

namespace Crypto.Tools.Base
{
    public interface ICryptionBase
    {
        public string[] SplitMessage(string text);
        public bool IsEmpty(string text);
        public string ReturnResultMessage(bool isEncryption);
    }
}
