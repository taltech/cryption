﻿using Crypto.Tools.Base;
using System;

namespace Crypto.Tools.Ciphers
{
    public sealed class ArturCipher : KeyCryption
    {
        public ArturCipher(string key, string message) : base(key, message) =>
            (Key, Message) = (key, message);

        public string ChangeLetters(bool isEncryption)
        {
            int newIndex, keyIndex;
            for (int i = 0; i < MessageIndices.Length; i++)
            {
                (newIndex, keyIndex) = (MessageIndices[i], KeyIndices[i % KeyIndices.Length]);
                newIndex += isEncryption ? keyIndex : Symbols.Length - keyIndex;
                Result += Symbols[newIndex % Symbols.Length];
            }

            return Result;
        }

        public override string ReturnResultMessage(bool isEncryption)
        {
            ChangeLetters(isEncryption);
            return base.ReturnResultMessage(isEncryption);
        }
    }
}
