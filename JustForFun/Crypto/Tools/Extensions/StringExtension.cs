﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Crypto.Tools.Extensions
{
    public static class StringExtension
    {
        public static int[] ToInts(this string letters, string alphabet) => letters.Select(x => alphabet.IndexOf(x)).ToArray();
    }
}
