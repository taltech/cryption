﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Crypto.Tools.Ciphers;

namespace Crypto
{
    public partial class Form1 : Form
    {
        private const string OutputFilePath = @"../../../output/";

        public Form1()
        {
            InitializeComponent();
            FillCiphersList();
        }

        private void FillCiphersList()
        {
            string[] ciphers = new[] { "Artur Cipher" };
            CiphersList.Items.AddRange(ciphers);
        }
        

        private void Encypt_Click(object sender, EventArgs e)
        {
            // ShowText(Color.Red, "You should add some text to encrypt");
            using (StreamWriter writer = new StreamWriter(OutputFilePath + "encrypted.txt"))
            {
                ArturCipher arturCipher = new ArturCipher(KeyBox.Text, InputBox.Text);
                string encrypted = arturCipher.ReturnResultMessage(true);
                writer.Write(encrypted);
                ShowText(Color.Aqua, encrypted);
            }
        }

        private void ShowText(Color color, string text)
            => (OutputBox.ForeColor, OutputBox.Text) = (color, text);

        private void Decrypt_Click(object sender, EventArgs e)
        {
            using (StreamWriter writer = new StreamWriter(OutputFilePath + "decrypted.txt"))
            {
                ArturCipher arturCipher = new ArturCipher(KeyBox.Text, InputBox.Text);
                string decrypted = arturCipher.ReturnResultMessage(false);
                writer.Write(decrypted);
                ShowText(Color.LawnGreen, decrypted);
            }
        }
    }
}